//
//  ViewController.swift
//  Cycle
//
//  Created by Aleksey Knysh on 1/13/21.
//  Copyright © 2021 Tsimafei Harhun. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var distanceTraveled: Double = 0
    var rest = 3.0
    var sumWater: Double = 0
    
    let heartFailure = Double.random(in: 80...90)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Создаём цикл с дистанцией 99 км
        
        while distanceTraveled < 99 {
            
            // Проезжаем на автобусе 20 км в два раза быстрей
            
            if distanceTraveled >= 40, distanceTraveled <= 60 {
                print("Едем на автобусе \(distanceTraveled) км\n")
                distanceTraveled += 0.5 * 4
            } else {
                
                // Если не едем на автобусе, то проходим дистанцию по 0,5 км и каждый раз пьём воду 200 мл. и подсчитываем сколько выпито
                
                distanceTraveled += 0.5
                sumWater += 0.2
                print("Выпито воды 200 мл.")
                
                // Выводим отдых, каждые 3 км и пройденную дистанцию по 1 км
                
                if distanceTraveled.truncatingRemainder(dividingBy: Double(3)) == 0 {
                    print("Пройденный путь \(distanceTraveled) км. отдыхаем \n")
                } else if distanceTraveled.truncatingRemainder(dividingBy: Double(1)) == 0 {
                    print("Пройденный путь \(distanceTraveled) км. \n")
                }
                
                // Округляем рандомное число, на каком км. будет разрыв сердца, сравниваем с дистанцией и выводим. После чего выходим из цикла
                
                if distanceTraveled == heartFailure.rounded(.toNearestOrAwayFromZero) {
                    print("--------------Разрыв сердца \(distanceTraveled) км--------------")
                    break
                }
            }
        }
    }
}
